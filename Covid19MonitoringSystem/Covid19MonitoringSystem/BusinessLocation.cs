﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class BusinessLocation
    {
        //properties
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }

        //constructors
        public BusinessLocation() { }
        public BusinessLocation(string bname, string bc, int mc)
        {
            BusinessName = bname;
            BranchCode = bc;
            MaximumCapacity = mc;
        }
        //method for calculating the capacity of the location
        public bool IsFull()
        {
            if (VisitorsNow == MaximumCapacity)
            {
                return true;
            }
            return false;
        }
        public override string ToString()
        {
            return "Business Name: " + BusinessName +
                "\tBranch Code: " + BranchCode +
                "\tMaximum Capacity: " + MaximumCapacity;
        }
    }
}
