﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    abstract class Person
    {
        //Properties
        public string Name { get; set; }

        //Lists
        public List<SafeEntry> safeEntryList = new List<SafeEntry>();
        public List<TravelEntry> travelEntryList = new List<TravelEntry>();

        //Constructors
        public Person() { }
        public Person(string name)
        {
            Name = name;
        }

        //Unique Methods
        public void AddTravelEntry(TravelEntry travelEntry) //travelentry for TravelEnntry
        {
            travelEntryList.Add(travelEntry);
        }
        public void AddSafeEntry(SafeEntry safeEntry) //safeentry for SafeEntry
        {
            safeEntryList.Add(safeEntry);
        }

        //Abstract Methods
        public abstract double CalculateSHNCharges();
        
        //To string method
        public override string ToString()
        {
            return "Name: " + Name;
        }

    }
}
