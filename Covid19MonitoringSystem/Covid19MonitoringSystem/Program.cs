﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json; //for api
using System.IO; //for csv files
using System.Collections.Generic; //for lists
using System.Globalization;

namespace Covid19MonitoringSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            //lists - Person, BusinessLocation, SHNFacility
            List<Person> personList = new List<Person>();
            List<BusinessLocation> businessLocationList = new List<BusinessLocation>();
            List<SHNFacility> shnFacilityList = new List<SHNFacility>();
            List<TraceTogetherToken> traceTogetherTokenList = new List<TraceTogetherToken>();

            //v2: vacancy count
            List<int> vacCount = new List<int>(4);
            vacCount.Add(35);
            vacCount.Add(20);
            vacCount.Add(100);
            vacCount.Add(1);


            //initialize Person objects & TravelEntry & TraceTogetherToken & Business Location objects
            InitObjects(personList, traceTogetherTokenList, vacCount);
            InitBusinessLocation(businessLocationList);

            //initialize ShnFacility
            using (HttpClient client = new HttpClient()) //api call
            {
                // GET request
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net/");
                Task<HttpResponseMessage> responseTask = client.GetAsync("facility");
                responseTask.Wait();

                // retrieve response
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;

                    // Convert JSON text string to object
                    shnFacilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                }
            }

            Console.WriteLine("Token List"); //Dion & Tom have
            foreach (TraceTogetherToken t in traceTogetherTokenList)
            {
                Console.WriteLine(t);
            }

            Console.WriteLine("SHN List");
            foreach (SHNFacility s in shnFacilityList)
            {
                Console.WriteLine(s);
            }


            //menu
            while (true)
            {
                DisplayMenu();

                try
                {
                    Console.Write("Enter your option: ");
                    int opt = Convert.ToInt32(Console.ReadLine());

                    if (opt == 0)
                    {
                        Console.Write("Bye Bye!");
                        break;
                    }
                    else if (opt == 1)
                    {
                        PrintVisitor(personList);
                        Console.WriteLine("");
                    }
                    else if (opt == 2)
                    {
                        PrintResident(personList);
                        Console.WriteLine("");
                    }
                    else if (opt == 3)
                    {
                        PrintPerson(personList, traceTogetherTokenList);
                        Console.WriteLine("");
                    }
                    else if (opt == 4)
                    {
                        AssignReplaceToken(personList, traceTogetherTokenList);
                        Console.WriteLine("");
                    }
                    else if (opt == 5)
                    {
                        ListBusinessLoc(businessLocationList);
                        Console.WriteLine("");
                    }
                    else if (opt == 6)
                    {
                        EditBusinessCap(businessLocationList);
                        Console.WriteLine("");
                    }
                    else if (opt == 7)
                    {
                        CheckIn(personList, businessLocationList);
                        Console.WriteLine("");
                    }
                    else if (opt == 8)
                    {
                        CheckOut(personList, businessLocationList);
                        Console.WriteLine("");
                    }
                    else if (opt == 9)
                    {
                        ListShnFac(shnFacilityList, vacCount);
                        Console.WriteLine("");
                    }
                    else if (opt == 10)
                    {
                        CreateVisitor(personList);
                        Console.WriteLine("");
                    }
                    else if (opt == 11)
                    {
                        CreateTravelEntryRecord(personList, shnFacilityList, vacCount);
                        Console.WriteLine("");
                    }
                    else if (opt == 12)
                    {
                        CalculateShnCharges(shnFacilityList, personList);
                        Console.WriteLine("");
                    }
                    else if (opt == 13)
                    {
                        ContactTracingReport(businessLocationList, personList);
                        Console.WriteLine("");
                    }
                    else if (opt == 14)
                    {
                        ShnStatusReport(shnFacilityList, personList);
                        Console.WriteLine("");
                    }
                    else
                    {
                        Console.WriteLine("Try again!\n");
                    }
                }

                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }

            }

        }



        // print menu [done. tested.] //v2: added [0] Exit
        static void DisplayMenu()
        {
            Console.WriteLine("----- Menu -----");
            Console.WriteLine("[0] Exit");
            Console.WriteLine("[1] List Visitor Details");
            Console.WriteLine("[2] List Resident Details");
            Console.WriteLine("[3] List Person Details");
            Console.WriteLine("[4] Assign/Replace TraceTogether Token");
            Console.WriteLine("[5] List all Business Locations");
            Console.WriteLine("[6] Edit Business Location Capacity");
            Console.WriteLine("[7] SafeEntry Check-in");
            Console.WriteLine("[8] SafeEntry Check-out");
            Console.WriteLine("[9] List all SHN Facilities ");
            Console.WriteLine("[10] Create Visitor");
            Console.WriteLine("[11] Create TravelEntry Record");
            Console.WriteLine("[12] Calculate SHN Charges");
            Console.WriteLine("[13] Contact Tracing Reporting");
            Console.WriteLine("[14] SHN Status Reporting");
            Console.WriteLine("----------------");
        }

        // Initialize person objects [done. tested.] //v2: initialized travelentry, tracetogethertokens here
        static void InitObjects(List<Person> pList, List<TraceTogetherToken> tokList, List <int> vacCount)
        {
            string[] lines = File.ReadAllLines("Person.csv");

            for (int i = 1; i < lines.Length; i++)
            {
                string[] data = lines[i].Split(",");

                //0type,1name,2address,3lastLeftCountry,4passportNo,5nationality,6tokenSerial,
                //7tokenCollectionLocation,8tokenExpiryDate,9travelEntryLastCountry,10travelEntryMode,
                //11travelEntryDate,12travelShnEndDate,13travelIsPaid,14facilityName

                //init Person objects
                string type = data[0];
                string name = data[1];

                //if (data[3] == "") //lastleft country

                if (type == "visitor")
                {
                    string passport = data[4];
                    string nationality = data[5];

                    //Visitor(string Pno, string n, string name)
                    Person newVis = new Visitor(passport, nationality, name);
                    pList.Add(newVis);

                    //v2: init travelentry objects if have
                    if (data[11] != "") //if entry date is not null, make travel entry object
                    {
                        string lastCountry = data[9];
                        string entryMode = data[10];
                        DateTime entryDate = Convert.ToDateTime(data[11]);
                        DateTime shnEnd = Convert.ToDateTime(data[12]);
                        bool paid = Convert.ToBoolean(data[13]);
                        //string facName = data[14];

                        //TravelEntry(string lastCountry, string em, DateTime ed, DateTime shnEnd, bool ip) 
                        TravelEntry newTravelEntry = new TravelEntry(lastCountry, entryMode, entryDate, shnEnd, paid);
                        newVis.AddTravelEntry(newTravelEntry);
                    }

                }

                else //(type == "resident")
                {
                    string addr = data[2];
                    DateTime lastLeft = Convert.ToDateTime(data[3]);

                    //Resident(string name, string adr, DateTime lastleft)
                    Person newRes = new Resident(name, addr, lastLeft);
                    pList.Add(newRes);

                    //v2: init token objects
                    if (data[8] != "" && data[6] != "") //if exp date not null, have token
                    {
                        string sNo = data[6];
                        string loc = data[7];
                        DateTime expDate = Convert.ToDateTime(data[8]);

                        TraceTogetherToken newToken = new TraceTogetherToken(sNo, loc, expDate);
                        tokList.Add(newToken);
                    }

                    //v2: init travelentry objects if have
                    if (data[11] != "") //if entry date is not null, make travel entry object
                    {
                        string lastCountry = data[9];
                        string entryMode = data[10];
                        DateTime entryDate = Convert.ToDateTime(data[11]);
                        DateTime shnEnd = Convert.ToDateTime(data[12]);
                        bool paid = Convert.ToBoolean(data[13]);

                        //TravelEntry(string lastCountry, string em, DateTime ed, DateTime shnEnd, bool ip) 
                        TravelEntry newTravelEntry = new TravelEntry(lastCountry, entryMode, entryDate, shnEnd, paid);
                        newRes.AddTravelEntry(newTravelEntry);
                    }
                }

                //v2: update vaccount
                string facilityName = data[14];
                if (facilityName != "")
                {
                    if (facilityName == "A'Resort")
                    {
                        vacCount[0] -= 1;
                    }
                    else if (facilityName == "Yozel")
                    {
                        vacCount[1] -= 1;
                    }
                    else if (facilityName == "Mandarin Orchid")
                    {
                        vacCount[2] -= 1;
                    }
                    else if (facilityName == "Small Hostel")
                    {
                        vacCount[3] -= 1;
                    }
                }


            }

        }

        // Initialize Business Locations [done. tested.]
        static void InitBusinessLocation(List<BusinessLocation> bList)
        {
            //csv file - Person

            string[] lines = File.ReadAllLines("BusinessLocation.csv");

            for (int i = 1; i < lines.Length; i++)
            {
                string[] data = lines[i].Split(",");

                //businessname,branchcode,maximumcapacity
                string name = data[0];
                string branchCode = data[1];
                int maxCap = Convert.ToInt32(data[2]);

                //BusinessLocation(string bname, string bc, int mc)
                bList.Add(new BusinessLocation(name, branchCode, maxCap));
            }
        }

        // v2: Search Token
        static TraceTogetherToken SearchToken(List<Person> pList, string sNo)
        {
            foreach (Person p in pList)
            {
                if (p is Resident)
                {
                    Resident r = (Resident)p;

                    if (r.Token.SerialNo == sNo)
                    {
                        return r.Token; //return token
                    }
                }
            }
            return null;
        }
            
        // Search Person [done. tested.]
        static Person SearchPerson(List<Person> pList, string name)
        {
            foreach (Person p in pList)
            {
                if (p.Name == name)
                {
                    return p;
                }
            }
            return null;
        }

        // Search Business Location [done. tested.]
        static BusinessLocation SearchBusiness(List<BusinessLocation> bList, string code)
        {
            foreach (BusinessLocation b in bList)
            {
                if (b.BranchCode == code)
                {
                    return b;
                }
            }
            return null;
        }

        // Search SHN facility [done. not tested.]
        static SHNFacility SearchShnFac(List<SHNFacility> sList, string name)
        {
            foreach (SHNFacility s in sList)
            {
                if (s.FacilityName == name)
                {
                    return s;
                }
            }
            return null;
        }

        //Display Safe Entry List [done]
        static void PrintSafeEntryList(List<Person> pList, string name)
        {
            Console.WriteLine("\n~ Listing Safe Entry Details");

            Person person = SearchPerson(pList, name);

            //content
            foreach (SafeEntry s in person.safeEntryList)
            {
                Console.WriteLine("{0, -20} {1, -25} {2, -25}",
                    s.CheckIn, s.CheckOut, s.Location);
            }
            
        }


        //Display Travel Entry List [done] 
        static void PrintTravelEntryList(List<Person> pList, string name)
        {
            Console.WriteLine("\n~ Listing Travel Entry Details");

            Person person = SearchPerson(pList, name);

            //header
            Console.WriteLine("{0, -10} {1, -25} {2, -25}",
                    "Last Country Of Embarkation", "Entry Mode", "Entry Date");

            //content
            foreach (TravelEntry t in person.travelEntryList)
            {
                Console.WriteLine("{0, -25} {1, -20} {2, -25}",
                    t.LastCountryOfEmbarkation, t.EntryMode, t.EntryDate);
            }
     
        }

        //Display Trace Together Token List [done] v2: changed from person list to tlist instead
        //static void PrintToken(List<TraceTogetherToken> tList, string name)
        //{
        //    Console.WriteLine("\n~ Listing Trace Together Token Entry Details");

        //    //TraceTogetherToken(string sno, string cl, DateTime ep)

        //    //header
        //    Console.WriteLine("{0, -10} {1, -25} {2, -25}",
        //            "Serial No.", "Collection Location", "Expiry Date");

        //    //content
        //    foreach (TraceTogetherToken t in tList)
        //    {
        //        Console.WriteLine("{0, -25} {1, -10} {2, -25}",
        //            t.SerialNo, t.CollectionLocation, t.ExpiryDate);
        //    }
            

        //}

        // (j) 1. List Vistors Details [done. not tested.]
        static void PrintVisitor(List<Person> pList)
        {
            Console.WriteLine("\n~ Listing Visitor Details");

            List<Person> visitorList = new List<Person>();

            foreach (Person p in pList)
            {
                if (p is Resident)
                {
                    continue;
                }
                else
                {
                    Visitor v = (Visitor)p;
                    visitorList.Add(v);
                }
            }

            //Visitor(string Pno, string n, string name)

            //header
            Console.WriteLine("{0, -25} {1, -15} {2, -15}",
                    "Passport No", "Nationality", "Name");

            //content
            foreach (Visitor vis in visitorList)
            {
                Console.WriteLine("{0, -25} {1, -15} {2, -15}",
                    vis.PassportNo, vis.Nationality, vis.Name);

            }

        }

        // (j) 2. List Resident Details [done. not tested.]
        static void PrintResident(List<Person> pList)
        {
            Console.WriteLine("\n~ Listing Resident Details");

            List<Person> residentList = new List<Person>();

            foreach (Person p in pList)
            {
                if (p is Visitor)
                {
                    continue;
                }
                else
                {
                    Resident r = (Resident)p;
                    residentList.Add(r);
                }
            }

            //Resident(string n, string a, DateTime l)

            //header
            Console.WriteLine("{0, -10} {1, -25} {2, -25}",
                    "Name", "Address", "Last Left Country");

            //content
            foreach (Resident res in residentList)
            {
                Console.WriteLine("{0, -10} {1, -25} {2, -25}",
                    res.Name, res.Address, res.LastLeftCountry);
            }
        }

        // (m) 3. List Person Details [printing trace together token details is not done]
        static void PrintPerson(List<Person> pList, List<TraceTogetherToken> tList)
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("\n~ Listing Person Details");

                    //1.prompt user for name

                    Console.Write("Enter Name: ");
                    string name = Console.ReadLine();

                    //2.search for person
                    Person person = SearchPerson(pList, name);
                    if (person != null)
                    {
                        if (person is Visitor)
                        {
                            //3.list person details including TravelEntry and SafeEntry details
                            PrintSafeEntryList(pList, name);
                            PrintTravelEntryList(pList, name);
                            break;
                        }
                        else//person is resident
                        {
                            Resident resident = (Resident)person;

                            //3.list person details including TravelEntry and SafeEntry details
                            PrintSafeEntryList(pList, name);
                            PrintTravelEntryList(pList, name);


                            if (SearchToken(pList, resident.Token.SerialNo) != null)
                            {
                                
                                TraceTogetherToken tok = SearchToken(pList, resident.Token.SerialNo);

                                //header
                                Console.WriteLine("{0, -10} {1, -25} {2, -25}",
                                        "Serial No.", "Collection Location", "Expiry Date");

                                //content
                                Console.WriteLine("{0, -25} {1, -10} {2, -25}",
                                    tok.SerialNo, tok.CollectionLocation, tok.ExpiryDate);
                                break;
                            }
                            else
                            {
                                
                                break;
                            }

                        }
                    }
                    else
                    {
                        Console.WriteLine("Person Name Does not Exist\n");
                    }
                    break;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }
        }

        // (m) 4. Assign/Replace TraceTogether Token
        static void AssignReplaceToken(List<Person> pList, List<TraceTogetherToken> tList)
        {
            Console.WriteLine("\n~ Assign/Replace TraceTogether Token");

            while (true)
            {
                try
                {
                    //prompt user for name 
                    Console.Write("Enter Name: ");
                    string name = Console.ReadLine();

                    Person person = SearchPerson(pList, name); //return person object or null if person doesnt exist

                    if (person != null) //return person object
                    {
                        if (person is Visitor)
                        {
                            Console.WriteLine("Visitors cannot get a TraceTogether Token");
                        }

                        else //if resident
                        {
                            Resident resident = (Resident)person;

                            if (resident.Token != null) //if have, check if eligible for replacement
                            {
                                string serialNo = resident.Token.SerialNo;

                                if (resident.Token.IsEligibaleForReplacement()) //if eligible
                                {
                                    Console.WriteLine("Trace Together Token is Eligible for Replacement.\n");

                                    while (true)
                                    {
                                        try
                                        {
                                            Console.Write("\nEnter new Trace Together Token Serial number: ");
                                            string newSNo = Console.ReadLine();

                                            foreach (TraceTogetherToken t in tList)
                                            {
                                                if (newSNo == t.SerialNo) //in use 
                                                {
                                                    Console.WriteLine("Serial Number has been used.");
                                                }
                                                else //not in use
                                                {
                                                    tList.Remove(resident.Token); //old token removed

                                                    Console.Write("Enter Collection Location: ");
                                                    string loc = Console.ReadLine();

                                                    resident.Token.ReplaceToken(newSNo, loc); //uploaded token added
                                                    tList.Add(resident.Token);

                                                    //v2: print token list
                                                    //header
                                                    Console.WriteLine("{0, -10} {1, -25} {2, -25}",
                                                            "Serial No.", "Collection Location", "Expiry Date");

                                                    //content
                                                    Console.WriteLine("{0, -25} {1, -10} {2, -25}",
                                                        resident.Token.SerialNo, resident.Token.CollectionLocation, resident.Token.ExpiryDate);

                                                    Console.Write("Token Replaced Successfully.");
                                                    break;

                                                }
                                            }
                                        }
                                        catch (FormatException ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                            Console.WriteLine("Please try again.\n");
                                        }
                                    }
                                }
                                else //not eligible
                                {
                                    Console.WriteLine("Trace Together Token not Eligible for Replacement.");
                                }

                            }
                            else //if resident has no existing token
                            {
                                //create and assign a TraceTogetherToken object if resident has no existing token
                                Console.WriteLine("Resident has no existing Trace Together Token.\n");

                                Console.Write("Enter new Trace Together Token Serial number: ");
                                string newSNo = Console.ReadLine();

                                Console.Write("Enter Collection Location: ");
                                string loc = Console.ReadLine();

                                tList.Add(resident.Token);

                                Console.WriteLine("Token added Successfully.");

                            }
                        }
                    }

                    else //if person null
                    {
                        Console.WriteLine("Person Does not Exist\n");
                    }

                    break;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }
        }

        // (j) 5. List all Business Locations[done]
        static void ListBusinessLoc(List<BusinessLocation> bList)
        {
            Console.WriteLine("\n~ Listing all Business Locations");

            //header
            Console.WriteLine("{0, -25} {1, -15} {2, -15} {3, -15}",
                    "Business Name", "Branch Code", "Maximum Capacity", "Visitors Now");

            //content
            foreach (BusinessLocation b in bList)
            {
                Console.WriteLine("{0, -25} {1, -15} {2, -15} {3, -15}",
                    b.BusinessName, b.BranchCode, b.MaximumCapacity, b.VisitorsNow);
            }

        }

        // (j) 6. Edit Business Location Capacity [done. error shld be fixed but hav not tested.]
        static void EditBusinessCap(List<BusinessLocation> bList)
        {
            Console.WriteLine("\n~ Editing Business Location Capacity");

            ListBusinessLoc(bList);
            Console.WriteLine("");


            while (true)
            {
                try
                {
                    //prompt user fpr branch code
                    Console.Write("Enter Branch Code: ");
                    string code = Console.ReadLine();

                    //search for business location
                    if (SearchBusiness(bList, code) != null)
                    {
                        BusinessLocation busLoc = SearchBusiness(bList, code);

                        while (true)
                        {
                            try
                            {
                                Console.Write("Enter Max Capacity: ");
                                int maxCap = Convert.ToInt32(Console.ReadLine());

                                //edit 
                                busLoc.MaximumCapacity = maxCap;

                                //list updated list
                                ListBusinessLoc(bList);

                                break;
                            }

                            catch (FormatException ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine("Please try again.\n");
                            }
                        }

                        break;

                    }

                    else
                    {
                        Console.WriteLine("Business Location does not exist.");
                    }

                }

                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }

        }

        // (m) 7. SafeEntry Check-in [] - Check 
        static void CheckIn(List<Person> pList, List<BusinessLocation> bList)
        {
            Console.WriteLine("\n~ SafeEntry Check-in");

            while (true)
            {
                try
                {
                    //prompt user for name 
                    Console.Write("Enter Name: ");
                    string name = Console.ReadLine();

                    //2.search for person
                    Person person = SearchPerson(pList, name); //return person object or null if person doesnt exist

                    

                    if (person != null) //return person object
                    {
                        while (true)
                        {
                            try
                            {
                                //3.list all business locations
                                ListBusinessLoc(bList);

                                //4.prompt user to select for business location to check -in 
                                Console.Write("Enter Branch Code of Location to Check-In (or 0 to go back to menu): ");
                                int branchCode = Convert.ToInt32(Console.ReadLine());
                                //v.2 Exit out of the Check-In Loop
                                if (branchCode == 0)
                                {
                                    break;
                                }
                                //v.2 Checking of Branch Code
                                else if (branchCode == 1001)
                                {
                                    BusinessLocation business = SearchBusiness(bList, Convert.ToString(branchCode));

                                    if (business != null)
                                    {
                                        if (business.IsFull() == false)
                                        {
                                            //5.create SafeEntry object if the location is not full
                                            //SafeEntry(DateTime checkin, BusinessLocation location)
                                            SafeEntry safeEntry1 = new SafeEntry(DateTime.Now, business);

                                            //increase visitorsNow count
                                            business.VisitorsNow += 1;

                                            //6.add SafeEntry object to person
                                            person.AddSafeEntry(safeEntry1);
                                            Console.WriteLine("\nSafeEntry for {0} Added successfully in Business Location {1}.", name, branchCode);

                                            //list all business locations
                                            ListBusinessLoc(bList);

                                            break;
                                        }

                                        else
                                        {
                                            Console.WriteLine("Business Location is at Maximum Capacity");
                                            Console.WriteLine("Please Wait\n");
                                            //v.2 Exit out of the Check-In Loop if Location is at Maximum Capacity
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Business Location Does Not Exist\n");
                                    }
                                }
                                //v.2 Checking of Branch Code
                                else if (branchCode == 3223)
                                {
                                    BusinessLocation business = SearchBusiness(bList, Convert.ToString(branchCode));

                                    if (business != null)
                                    {
                                        if (business.IsFull() == false)
                                        {
                                            //5.create SafeEntry object if the location is not full
                                            //SafeEntry(DateTime checkin, BusinessLocation location)
                                            SafeEntry safeEntry1 = new SafeEntry(DateTime.Now, business);

                                            //increase visitorsNow count
                                            business.VisitorsNow += 1;

                                            //6.add SafeEntry object to person
                                            person.AddSafeEntry(safeEntry1);
                                            Console.WriteLine("\nSafeEntry for {0} Added successfully in Business Location {1}.", name, branchCode);

                                            //list all business locations
                                            ListBusinessLoc(bList);

                                            break;
                                        }

                                        else
                                        {
                                            Console.WriteLine("Business Location is at Maximum Capacity");
                                            Console.WriteLine("Please Wait\n");
                                            //v.2 Exit out of the Check-In Loop if Location is at Maximum Capacity
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Business Location Does Not Exist\n");
                                        break;
                                    }
                                }
                                //v.2 Checking of Branch Code
                                else if (branchCode == 9281)
                                {
                                    BusinessLocation business = SearchBusiness(bList, Convert.ToString(branchCode));

                                    if (business != null)
                                    {
                                        if (business.IsFull() == false)
                                        {
                                            //5.create SafeEntry object if the location is not full
                                            //SafeEntry(DateTime checkin, BusinessLocation location)
                                            SafeEntry safeEntry1 = new SafeEntry(DateTime.Now, business);

                                            //increase visitorsNow count
                                            business.VisitorsNow += 1;

                                            //6.add SafeEntry object to person
                                            person.AddSafeEntry(safeEntry1);
                                            Console.WriteLine("\nSafeEntry for {0} Added successfully in Business Location {1}.", name, branchCode);

                                            //list all business locations
                                            ListBusinessLoc(bList);

                                            break;
                                        }

                                        else
                                        {
                                            Console.WriteLine("Business Location is at Maximum Capacity");
                                            Console.WriteLine("Please Wait\n");
                                            //v.2 Exit out of the Check-In Loop if Location is at Maximum Capacity
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Business Location Does Not Exist\n");
                                        break;
                                    }
                                }
                                //v.2 Checking of Branch Code
                                else if (branchCode == 8231)
                                {
                                    BusinessLocation business = SearchBusiness(bList, Convert.ToString(branchCode));

                                    if (business != null)
                                    {
                                        if (business.IsFull() == false)
                                        {
                                            //5.create SafeEntry object if the location is not full
                                            //SafeEntry(DateTime checkin, BusinessLocation location)
                                            SafeEntry safeEntry1 = new SafeEntry(DateTime.Now, business);

                                            //increase visitorsNow count
                                            business.VisitorsNow += 1;

                                            //6.add SafeEntry object to person
                                            person.AddSafeEntry(safeEntry1);
                                            Console.WriteLine("\nSafeEntry for {0} Added successfully in Business Location {1}.", name, branchCode);

                                            //list all business locations
                                            ListBusinessLoc(bList);

                                            break;
                                        }

                                        else
                                        {
                                            Console.WriteLine("Business Location is at Maximum Capacity");
                                            Console.WriteLine("Please Wait\n");
                                            //v.2 Exit out of the Check-In Loop if Location is at Maximum Capacity
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Business Location Does Not Exist\n");
                                        break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("You have entered the Wrong Branch Code");
                                    break;
                                }
                            }
                            catch (FormatException ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine("Please try again.\n");
                            }
                        }
                    }
                    else //if person not null
                    {
                        Console.WriteLine("\nPerson Does not Exist\n");
                    }
                    break;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }

        }

        // (m) 8. SafeEntry Check-out [Done Tested]
        static void CheckOut(List<Person> pList, List<BusinessLocation> bList)
        {
            Console.WriteLine("\n~ SafeEntry Check-Out");
            while (true)
            {
                try
                {
                    //prompt user for name 
                    Console.Write("Enter Name: ");
                    string name = Console.ReadLine();

                    //2.search for person
                    Person person = SearchPerson(pList, name); //return person object or null if person doesnt exist

                    if (person != null)
                    {

                        foreach (SafeEntry s in person.safeEntryList)
                        {
                            if (s.CheckOut != null)
                            {
                                //3.list SafeEntry records for that person that have not been checked-out 
                                PrintSafeEntryList(pList, name);

                                //4.prompt user to select record to check -out 
                                Console.Write("Enter Brach Code of Location to Check-Out (or 0 to go back to menu): ");
                                int checkOutPlace = Convert.ToInt32(Console.ReadLine());

                                //v.2 Exit out of the Check-In Loop
                                if (checkOutPlace == 0)
                                {
                                    break;
                                }
                                else
                                {
                                    //5.call PerformCheckOut() to check -out, and reduce visitorsNow by 1
                                    s.PerformCheckOut();

                                    //list all business locations
                                    Console.WriteLine("");
                                    ListBusinessLoc(bList);

                                    Console.WriteLine("\nCheckOut Successful.");

                                    break;
                                }
                            }

                            else //null
                            {
                                Console.WriteLine("You have not been Checked In at any locations\n");
                                break;
                            }

                        }

                    }

                    else //if person not null
                    {
                        Console.WriteLine("Person Does not Exist\n");
                    }

                    break;

                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }

        }

        //(j) 9.  List all SHN Facilities [done] //vs: used global variable for facility vacancy.
        static void ListShnFac(List<SHNFacility> sList, List<int>vacCount)
        {
            Console.WriteLine("\n~ Listing all SHN Facilities");

            //header
            Console.WriteLine("{0, -15} {1, -15} {2, -15} {3, -15} {4, -15} {5, -15}",
                "Facility Name", "Facility Vacancy", "Facility Capacity", "DistFromSeaCheckpoint", "DistFromLandCheckpoint", "DistFromAirCheckpoint");


            //content
            for (var i = 0; i <= sList.Count; i++) //v2: added for list
            {
                //s.FacilityVacancy = s.FacilityCapacity;
                Console.WriteLine("{0, -15} {1, -15} {2, -25} {3, -25} {4, -25} {5, -25}",
                sList[i].FacilityName, vacCount[i], sList[i].FacilityCapacity, sList[i].DistFromSeaCheckpoint, sList[i].DistFromLandCheckpoint, sList[i].DistFromAirCheckpoint);
            }

        }

        // (m) 10. Create Visitor [done]
        static void CreateVisitor(List<Person> pList)
        {

            Console.WriteLine("\n~ Create Visitor Record");

            Console.Write("Enter Name: ");
            string name = Console.ReadLine();
            Console.Write("Enter Passport Number: ");
            string passport = Console.ReadLine();
            Console.Write("Enter Nationality: ");
            string nationality = Console.ReadLine();


            pList.Add(new Visitor(passport, nationality, name));
            Console.WriteLine("Visitor {0} is added.", name);

            PrintVisitor(pList);
        }

        // (j) 11. Create TravelEntry Record [done. not tested.] 
        static void CreateTravelEntryRecord(List<Person> pList, List<SHNFacility> sList, List<int>vacCount)
        {
            Console.WriteLine("\n~ Create TravelEntry Record");

            while (true)
            {
                try
                {
                    //prompt user for name
                    Console.Write("Enter your name: ");
                    string name = Console.ReadLine();

                    //search for person
                    Person person = SearchPerson(pList, name);

                    if (person != null)
                    {
                        //prompt user for details(last country of embarkation, entry mode)
                        Console.Write("Enter last country of embarkation: ");
                        string lastCountry = Console.ReadLine();

                        while (true)
                        {
                            try
                            {
                                Console.Write("Enter entry mode (Air/Land/Water): ");
                                string entryMode = Console.ReadLine();

                                if (entryMode == "Air" || entryMode == "Water" || entryMode == "Land")
                                {
                                    //create TravelEntry object - Travelentry(string LastCountryOfEmbarkation, string entryMode, DateTime entryDate, DateTime ShnEnd, bool ispaid) 
                                    TravelEntry travelEntry = new TravelEntry(lastCountry, entryMode, DateTime.Now, DateTime.Now, false);

                                    //call CalculateSHNDuration() to calculate SHNEndDate based on criteria given in the background brief
                                    travelEntry.CalculateSHNDuration(); //update shn end date depending on LastCountryOfEmbarkation

                                    if (lastCountry == "New Zealand" || lastCountry == "Vietnam")
                                    {
                                        Console.WriteLine("\nLast Country Of Embarkation is either Vietnam or New Zealand\n" +
                                            "Thus, your Entry Arrangements are No SHN & Swab Test.");

                                        break;
                                    }

                                    else if (lastCountry == "Macao")
                                    {
                                        Console.WriteLine("\nLast Country Of Embarkation is Macao.\n" +
                                            "Thus, your Entry Arrangements 7-day SHN at own accomodation & Swab Test.");

                                        break;
                                    }

                                    else
                                    {
                                        Console.WriteLine("\nLast Country Of Embarkation is not Macao, New Zealand or Vietnam.\n" +
                                            "Thus, your Entry Arrangements 14-day SHN at a vacant SHN Facility & Swab Test.\n");

                                        //list SHN facilities 
                                        ListShnFac(sList, vacCount);

                                        //prompt user to select facility
                                        while (true)
                                        {
                                            try
                                            {
                                                Console.Write("Enter facility name: ");
                                                string facName = Console.ReadLine();

                                                //search for facility
                                                if (SearchShnFac(sList, facName) != null)
                                                {
                                                    SHNFacility shnFac = SearchShnFac(sList, facName);

                                                    //assign chosen SHN facility
                                                    if (shnFac.IsAvailable())
                                                    {
                                                        travelEntry.AssignSHNFacility(shnFac);

                                                        //reduce the vacancy count

                                                        shnFac.FacilityVacancy -= 1;

                                                        //call AddTravelEntry() in Person to assign the TravelEntry object
                                                        person.AddTravelEntry(travelEntry);

                                                        Console.WriteLine("TravelEntry for {0} Created.", name);

                                                        //list updated list
                                                        ListShnFac(sList, vacCount);

                                                        break;
                                                    }

                                                    else
                                                    {
                                                        Console.WriteLine("SHN Facility not available.");
                                                    }

                                                }

                                                else
                                                {
                                                    Console.WriteLine("SHN facility does not exist.\n");
                                                }
                                            }

                                            catch (FormatException ex)
                                            {
                                                Console.WriteLine(ex.Message);
                                                Console.WriteLine("Please try again.\n");
                                            }
                                        }
                                        break;

                                    }
                                }

                                else
                                {
                                    Console.WriteLine("Entry Mode not recognized. Choose from Air/Land/Water.");
                                }
                            }
                            catch (FormatException ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine("Please try again.\n");
                            }
                        }
                    }

                    else
                    {
                        Console.WriteLine("Person does not exist.");
                    }

                    break;

                }

                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }


        }

        //(m) Calculate SHN Charges [done]
        static void CalculateShnCharges(List<SHNFacility> sList, List<Person> pList)
        {
            Console.WriteLine("\n~ Calculating SHN Charges");

            while (true)
            {
                try
                {
                    double shnCharge = 0; //no gst
                    double total = 0; //with gst

                    //prompt user for name
                    Console.Write("Enter your name: ");
                    string name = Console.ReadLine();

                    //search for person
                    Person person = SearchPerson(pList, name); //return null or person

                    if (person != null) //check if person exist
                    {
                        if (person is Visitor) //check if person is visitor or resident
                        {
                            //downcast
                            Visitor visitor = (Visitor)person;

                            //retrieve TravelEntry with SHN ended and is unpaid
                            foreach (TravelEntry travelEntry in person.travelEntryList)
                            {
                                if (travelEntry != null)
                                {
                                    if (travelEntry.IsPaid == false && travelEntry.ShnEndDate > DateTime.Now)
                                    {
                                        Console.WriteLine("Person has a Travel Entry.");

                                        //calculate shnCharge
                                        if (travelEntry.ShnStay.FacilityName != null) //if it is assigned a FacilityName, it is serving 14-day
                                        {
                                            double tport = travelEntry.ShnStay.CalculateTravelCost(travelEntry.EntryMode, travelEntry.EntryDate);

                                            Console.WriteLine("\nSHN Mode is 14-day SHN at SDF." +
                                                "The following charges applicable is $2000 for SDF Charge + ${0} for Transportation Cost + $200 for Swab Test. GST Exclusive.", tport);

                                            //calculate transportation fare + sdf charge 0f 2000
                                            shnCharge = visitor.CalculateSHNCharges() + tport + 2000;
                                        }

                                        else //either SHN mode is none or own accomodation
                                        {
                                            if (travelEntry.LastCountryOfEmbarkation == "New Zealand" || travelEntry.LastCountryOfEmbarkation == "Vietname") //no shn
                                            {
                                                Console.WriteLine("\nSHN Mode is No SHN." +
                                                "The following charges applicable is $80 for Transportation Cost + $200 for Swab Test. GST Exclusive.");

                                                shnCharge = 20; //only the transport fee
                                            }
                                            else //own accomodation
                                            {
                                                Console.WriteLine("\nSHN Mode is 7-day SHN at own accommodation." +
                                                "The following charges applicable is $80 for Transportation Cost + $200 for Swab Test. GST Exclusive.");

                                                shnCharge = visitor.CalculateSHNCharges();
                                            }
                                        }

                                        //calculate total - add 7% GST
                                        total = 1.07 * shnCharge;

                                        Console.WriteLine("\nYour total amount is ${0}\n", total);

                                        while (true)
                                        {
                                            try
                                            {
                                                //prompt to make payment
                                                Console.WriteLine("Ready to make payment? (true/false): ");
                                                bool ready = Convert.ToBoolean(Console.ReadLine());

                                                if (ready)
                                                {
                                                    //change the isPaid Boolean value
                                                    travelEntry.IsPaid = true;

                                                    Console.WriteLine("Payment of {0} Successful.", total);

                                                    break;
                                                }
                                            }
                                            catch (FormatException ex)
                                            {
                                                Console.WriteLine(ex.Message);
                                                Console.WriteLine("Please try again.\n");
                                            }
                                        }
                                    }

                                    else
                                    {
                                        Console.WriteLine("Visitor with TravelEntry with SHN has not ended or is paid already");
                                        break;
                                    }
                                    
                                }
                                else
                                {
                                    Console.WriteLine("Visitor does not have a Travel Entry.");
                                    break;
                                }

                            }
                        }

                        else//if (person is Resident) //check if person is visitor or resident
                        {
                            //downcast
                            Resident resident = (Resident)person;

                            //retrieve TravelEntry with SHN ended and is unpaid
                            foreach (TravelEntry travelEntry in person.travelEntryList)
                            {
                                if (travelEntry != null)
                                {
                                    if (travelEntry.IsPaid == false && travelEntry.ShnEndDate > DateTime.Now)
                                    {
                                        //calculate shnCharge
                                        if (travelEntry.ShnStay.FacilityName != null) //if it is assigned a FacilityName, it is serving 14-day
                                        {
                                            Console.WriteLine("\nSHN Mode is 14-day SHN at SDF." +
                                                "The following charges applicable is $1000 for SDF Charge + $20 for Transportation Cost + $200 for Swab Test. GST Exclusive.");

                                            //calculate transportation fare + sdf charge 0f 1000 + trasnport of 20
                                            shnCharge = resident.CalculateSHNCharges() + 20 + 1000;
                                        }
                                        else //either SHN mode is none or own accomodation
                                        {
                                            if (travelEntry.LastCountryOfEmbarkation == "New Zealand" || travelEntry.LastCountryOfEmbarkation == "Vietname") //no shn
                                            {

                                                Console.WriteLine("\nSHN Mode is No SHN." +
                                                "The following charges applicable $200 for Swab Test. GST Exclusive.");
                                                shnCharge = 0; //only the transport fee
                                            }
                                            else //own accomodation
                                            {
                                                Console.WriteLine("\nSHN Mode is 7-day SHN at own accommodation." +
                                                "The following charges applicable is $20 for Transportation Cost + $200 for Swab Test. GST Exclusive.");
                                                shnCharge = resident.CalculateSHNCharges();
                                            }
                                        }

                                        //calculate total - add 7% GST
                                        total = 1.07 * shnCharge;

                                        Console.WriteLine("\nYour total amount is ${0}\n", total);

                                        while (true)
                                        {
                                            try
                                            {
                                                //prompt to make payment
                                                Console.WriteLine("Ready to make payment? (true/false): ");
                                                bool ready = Convert.ToBoolean(Console.ReadLine());

                                                if (ready)
                                                {
                                                    //change the isPaid Boolean value
                                                    travelEntry.IsPaid = true;

                                                    Console.WriteLine("Payment of {0} Successful.", total);

                                                    break;
                                                }
                                            }
                                            catch (FormatException ex)
                                            {
                                                Console.WriteLine(ex.Message);
                                                Console.WriteLine("Please try again.\n");
                                            }
                                        }
                                    }

                                    else
                                    {
                                        Console.WriteLine("Resident with TravelEntry with SHN has not ended or is paid already");
                                        break;
                                    }
                                    
                                }
                                else
                                {
                                    Console.WriteLine("Resident does not have a Travel Entry.");
                                    break;
                                }
                            }
                        }
                    }

                    else
                    {
                        Console.WriteLine("Person does not exist.");
                    }
                    Console.WriteLine("Person does not exist. Break");

                    break;
                }

                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }

        }

        //static void CalculateShnCharges2(List<SHNFacility> sList, List<Person> pList)
        //{
        //    Console.WriteLine("\n~ Calculating SHN Charges");
        //    while(true)
        //    {
        //        try
        //        {

        //        }
        //        catch (FormatException ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //            Console.WriteLine("Please try again.\n");
        //        }
        //    }
        //}

            // (m) 13. Contact Tracing Reporting(advanced)
            static void ContactTracingReport(List<BusinessLocation> bList, List<Person> pList)
        {
            while (true)
            {
                try
                {
                    //Prompt user for business name / branch code

                    Console.Write("Enter Branch Code: ");
                    string code = Console.ReadLine();

                    if (SearchBusiness(bList, code) != null)
                    {
                        BusinessLocation busLoc = SearchBusiness(bList, code);
                        //prompt for date and time
                        Console.Write("Enter Date and Time (DD/MM/YYYY HH:MM): ");
                        DateTime dateTime = Convert.ToDateTime(Console.ReadLine());

                        //• Generate a list of persons that are checked-in at that location and period.
                        List<Person> personCheckInList = new List<Person>();

                        foreach (Person p in pList)
                        {
                            foreach (SafeEntry safeEntry in p.safeEntryList)
                            {
                                if (dateTime < safeEntry.CheckOut)
                                {
                                    personCheckInList.Add(p);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }

                        if (personCheckInList == null)
                        {
                            Console.WriteLine("There are no People Checked-In during the Date Time given");
                        }
                        else
                        {
                            foreach (Person customer in personCheckInList)
                            {
                                string name = customer.Name;

                                foreach (SafeEntry safeEntry in customer.safeEntryList)
                                {
                                    DateTime checkOut = safeEntry.CheckOut;
                                    string loc = safeEntry.Location.BranchCode;
                                }

                                //write into CSV
                                string[] linesToWrite = { };
                                File.WriteAllLines("ContactTracingReport.csv", linesToWrite);

                            }
                        }

                        //• Export a CSV with details of their visit(e.g., check-in time, check -out time) 
                        //write into csv

                        //display csv report
                        Console.WriteLine("{0, -25} {1, -15} {2, -15}",
                            "Name", "Check-In Time", "Check-Out Time");

                        //content
                        string[] linesToRead = File.ReadAllLines("ShnStatusReport.csv");
                        foreach (string line in linesToRead)
                            Console.WriteLine("\t" + line);

                        break;

                    }
                    else
                    {
                        Console.WriteLine("The Branch code you have given does not Exist");
                    }

                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }
        }

        // (j) 14. SHN Status Reporting (advanced)
        static void ShnStatusReport(List<SHNFacility> sList, List<Person> pList)
        {
            while (true)
            {
                try
                {
                    //prompt user for date
                    Console.Write("Enter date (DD/MM/YYYY HH:MM): ");
                    DateTime date = Convert.ToDateTime(Console.ReadLine());

                    //search for travellers who is serving shn during the date given
                    List<Person> travellerServingList = new List<Person>(); //list of travellers who is serving shn during the date given

                    foreach (Person p in pList)
                    {
                        foreach (TravelEntry travelEntry in p.travelEntryList)
                        {
                            if (date < travelEntry.ShnEndDate)
                            {
                                travellerServingList.Add(p);
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    if (travellerServingList == null)
                    {
                        Console.WriteLine("There are no travellers serving shn during the date given");   
                    }
                    else
                    {
                        Console.WriteLine("There are travellers serving shn during the date given");
                        foreach (Person traveller in travellerServingList)
                        {
                            string name = traveller.Name;

                            foreach (TravelEntry travelEntry in traveller.travelEntryList)
                            {
                                DateTime endDate = travelEntry.ShnEndDate;
                                string loc = travelEntry.ShnStay.FacilityName;
                            }

                            //write into csv
                            string[] linesToWrite = { };
                            File.WriteAllLines("ShnStatusReport.csv", linesToWrite);

                        }
                    }
                   
                    //display csv report
                    Console.WriteLine("{0, -10} {1, -15} {2, -15}",
                        "Name", "SHN End Date", "SHN Location Served");

                    //content
                    string[] linesToRead = File.ReadAllLines("ShnStatusReport.csv");
                    foreach (string line in linesToRead)
                        Console.WriteLine("\t" + line);

                    break;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Please try again.\n");
                }
            }
        }
    }
}
