﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class Resident: Person
    {
        //properties
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; } 

        //constructors
        //Resident(string name, string adrr, DateTime lastleft)
        public Resident(string n, string a, DateTime l): base(n)
        {
            Address = a;
            LastLeftCountry = l;
        }

        //methods
        public override double CalculateSHNCharges() 
        {

            //double cost = 200;
            //double tport = 20;
            //double duration = travelEntryList[travelEntryList.Count - 1].shnEndDate.Subtract(travelEntryList[travelEntryList.Count]);
            //if(duration == 0)
            //{
            //    cost = cost + 0;
            //}
            //else if(duration == 7)
            //{
            //    cost = cost + 20;
            //}
            //else//14 days
            //{
            //    cost = cost + 1000 + tport;
            //}
            //return cost;

            //assuming, it is 7-day SHN at own accomodation
            //if 14-day, add sdf & calculate tport in main program
            //calculate gst in main program
            int swabTest = 200;
            double tport = 80;

            return (swabTest + tport);
        }

        public override string ToString()
        {
            return "Address: " + Address +
                "\tLast Left Country: " + LastLeftCountry +
                "\tToken: " + Token;

        }
    }
}
