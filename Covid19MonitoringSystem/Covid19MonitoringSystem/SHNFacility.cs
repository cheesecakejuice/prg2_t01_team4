﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class SHNFacility
    {
        //properties
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }

        // Add API in the main programme

        //consctructors
        public SHNFacility() { }
        public SHNFacility(string fname, int cap, double air, double sea, double land)
        {
            FacilityName = fname;
            FacilityCapacity = cap; 
            DistFromAirCheckpoint = air;
            DistFromLandCheckpoint = land;
            DistFromSeaCheckpoint = sea;
        }

        //methods
        public double CalculateTravelCost(string mode, DateTime entryDate) //input parameters are entryMode and entryDate
        {
            double baseFare = 0;
            double surcharge = 1;

            //int entryHour = entryDate.Hour; 
            //int entryMinute = entryDate.Minute;

            if (mode == "air")
            {
                baseFare = 50 + (DistFromAirCheckpoint * 0.22);

                //Entry during: Midnight to 5.59 am - 50% 
                if ((entryDate.Hour <= 6) && (entryDate.Minute <= 59) && (entryDate.Hour < 1)) //&& (entryDate.Minute < 0)
                {
                    surcharge = 1.50;
                }
                //Entry during: 6 am to 8.59 am; or 6 pm to 11.59 pm - 25%
                else
                {
                    surcharge = 1.25;
                }
            }

            else if (mode == "land")
            {
                baseFare = 50 + (DistFromLandCheckpoint * 0.22);

                //Entry during: Midnight to 5.59 am - 50% 
                if ((entryDate.Hour <= 6) && (entryDate.Minute <= 59) && (entryDate.Hour < 1)) //&& (entryDate.Minute < 0)
                {
                    surcharge = 1.50;
                }
                //Entry during: 6 am to 8.59 am; or 6 pm to 11.59 pm - 25%
                else
                {
                    surcharge = 1.25;
                }
            }

            else if (mode == "sea")
            {
                baseFare = 50 + (DistFromSeaCheckpoint * 0.22);

                //Entry during: Midnight to 5.59 am - 50% 
                if ((entryDate.Hour <= 6) && (entryDate.Minute <= 59) && (entryDate.Hour < 1)) //&& (entryDate.Minute < 0)
                {
                    surcharge = 1.50;
                }
                //Entry during: 6 am to 8.59 am; or 6 pm to 11.59 pm - 25%
                else
                {
                    surcharge = 1.25;
                }
            }

            return baseFare * surcharge;
        }

        public bool IsAvailable()
        {
            if (FacilityCapacity < 1)
            {
                return false;
            }

            return true;
        }

        public override string ToString()
        {
            return "Facility Name: " + FacilityName +
                "\tFacility Capacity: " + FacilityCapacity +
                "\tFacility Vacancy: " + FacilityVacancy +
                "\tDistance From Air Checkpoint: " + DistFromAirCheckpoint +
                "\tDistance From Sea Checkpoint: " + DistFromSeaCheckpoint +
                "\tDistance From Land Checkpoint: " + DistFromLandCheckpoint;
        }

    }
}
