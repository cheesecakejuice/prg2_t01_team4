﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class SafeEntry
    {
        //properties
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }
        
        //constructors
        public SafeEntry() { }

        public SafeEntry(DateTime cin, BusinessLocation location)
        {
            CheckIn = cin;
            Location = location;
        }
        
        //method for check out
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
            Location.VisitorsNow -= 1;

        }

        //to string method
        public override string ToString()
        {
            return "CheckIn: " + CheckIn +
                "\tCheckOut: " + CheckOut +
                "\tLocation: " + Location;
        }
    }
}
