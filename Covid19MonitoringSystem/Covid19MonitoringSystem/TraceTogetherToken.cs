﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class TraceTogetherToken
    {
        //Properties
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        //Constructors
        public TraceTogetherToken() { }
        public TraceTogetherToken(string sno, string cl, DateTime ep)
        {
            SerialNo = sno;
            CollectionLocation = cl;
            ExpiryDate = ep;
        }
        
        //Methods
        public bool IsEligibaleForReplacement() 
        {
            int expiryMonth = ExpiryDate.Month;
            int expiryYear = ExpiryDate.Year;

            int nowMonth = (DateTime.Now).Month;
            int nowYear = (DateTime.Now).Year;

            if ((expiryMonth - nowMonth) == 1 && expiryYear == nowYear)
            {
                return true;
            }
            return false;
        }
        
        public void ReplaceToken(string updatedSNo, string loc)
        {
            SerialNo = updatedSNo;
            CollectionLocation = loc;

            //token expires 6 months after issuing
            int expiryMonth = ExpiryDate.Month;
            int expiryDay = ExpiryDate.Day;
            int expiryYear = ExpiryDate.Year;

            ExpiryDate = new DateTime(expiryYear, expiryMonth + 6, expiryDay);
        }

        public override string ToString()
        {
            return "Serial No: " + SerialNo + "\t" +
                "Collection Location: " + CollectionLocation + "\t" +
                "Expiry Date: " + ExpiryDate;
        }


    }
}
