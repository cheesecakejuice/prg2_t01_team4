﻿//============================================================
// Student Number : S10204975, (student id)
// Student Name : Juliana, Mitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class TravelEntry
    {
        //properties
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool IsPaid { get; set; }

        //constructors
        public TravelEntry() { }

        public TravelEntry(string l, string em, DateTime ed, DateTime s, bool ip) 
        {
            LastCountryOfEmbarkation = l;
            EntryMode = em;
            EntryDate = ed;
        }

        //methods
        public void AssignSHNFacility(SHNFacility fname) //name selected by user
        {
            ShnStay = fname;
        }

        public void CalculateSHNDuration()  
        {
            if (LastCountryOfEmbarkation == "New Zeland" || LastCountryOfEmbarkation == "Vietnam")
            {
                ShnEndDate = ShnEndDate.AddDays(0);
            }

            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                ShnEndDate = ShnEndDate.AddDays(7);
            }

            else
            {
                ShnEndDate = ShnEndDate.AddDays(14);
            }
        }

        //to string method
        public override string ToString()
        {
            return "Last Country Of Embarkation: " + LastCountryOfEmbarkation +
                "\tEntry Mode: " + EntryMode +
                "\tEntry Date: " + EntryDate +
                "\tStay at Home Notice End Date: " + EntryDate +
                "\tStay at Home Notice Stay: " + ShnStay +
                "\tIs it Paid: " + IsPaid;
        }

    }
}
