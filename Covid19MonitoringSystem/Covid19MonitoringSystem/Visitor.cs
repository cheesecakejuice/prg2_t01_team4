﻿//============================================================
// Student Number : S10204975, S10207330
// Student Name : Juliana, Madhumitha
//  Module group: T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19MonitoringSystem
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        //Constructor
        //Visitor(string PassportNo, string Nationality, string Name)
        public Visitor(string Pno, string n, string name)
        {
            PassportNo = Pno;
            Nationality = n;
            Name = name;
        }
        public override double CalculateSHNCharges()
        {
            //assuming, it is 7-day SHN at own accomodation
            //if 14-day, add sdf & calculate tport in main program
            //calculate gst in main program
            int swabTest = 200;
            double tport = 80;

            return (swabTest + tport);
        }

        //to string method
        public override string ToString()
        {
            return "Nationality: " + Nationality + 
                "\tPassport No: " + PassportNo + 
                "\tName: " + Name;
        }
    }
}
